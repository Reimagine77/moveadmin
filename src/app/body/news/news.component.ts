import { Component, OnInit, Input } from '@angular/core';
import {NewsService} from './news.service'

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  newsData;

  constructor( private newsService:NewsService) { }

  ngOnInit() {
    this.newsService.getNews().subscribe((data)=>{
      this.newsData = data;
      console.log(this.newsData);
    });
  }

  deleteNews(event){    
    alert('I am paremt'+event);
    console.log(event);
    this.newsService.deleteNewsItem(event).subscribe((data:any)=>{
      console.log(data.message);
      alert('Deleted Successfully');
    });
  }

}
