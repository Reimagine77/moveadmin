import { Component, OnInit } from '@angular/core';
import {NewsService} from '../news.service';
import { FormsModule, NgForm } from '@angular/forms';
import {NewsItem} from '../news-item'

@Component({
  selector: 'app-post-news',
  templateUrl: './post-news.component.html',
  styleUrls: ['./post-news.component.css']
})
export class PostNewsComponent implements OnInit {
  newsItem:NewsItem;

  constructor(private newsService:NewsService) { }

  ngOnInit() {
    //calling getter
    this.newsService.presentData.subscribe((data:NewsItem)=>{
      console.log(data);
      this.newsItem =data;
    });
  }

  onSubmit(newsItem: NgForm){
    console.log(newsItem );
    this.newsService.postNews(newsItem).subscribe((data)=>{
      alert(data.message);
      this.newsItem=null;
    });
  }

}
