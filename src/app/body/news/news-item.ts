export class NewsItem {
    title:string;
    description:string;
    imageURL:string;

    constructor(title,description, imageURL){
        if(title){
            this.title = title;
        } else{
            this.title = "Default Title";
        }
        this.description = description;
        this.imageURL = imageURL;
    }
}
