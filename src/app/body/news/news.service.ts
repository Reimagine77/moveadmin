import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { NewsItem } from './news-item';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  private dataSource = new BehaviorSubject<any>({});
  //getter
  presentData = this.dataSource.asObservable();

  constructor(public httpClient:HttpClient) { }

  public getNews(){
    return this.httpClient.get('http://movie-api-ng.herokuapp.com/user/get-news');
  }

  public deleteNewsItem(id){
    return this.httpClient.delete('http://movie-api-ng.herokuapp.com/user/delete-news/'+id);
  }

  public putNews(newsItem):Observable<any> {
    console.log(newsItem);
    let body = {
      "title": newsItem.title,
      "description": newsItem.description,
      "imageURL": newsItem.imageURL
    };
    return this.httpClient.put('http://movie-api-ng.herokuapp.com/user/put-news/'+newsItem._id, body);
  };

  public postNews(newsItem):Observable<any> {
    console.log(newsItem);
    let body = {
      "title": newsItem.title,
      "description": newsItem.description,
      "imageURL": newsItem.imageURL
    };
    return this.httpClient.post('http://movie-api-ng.herokuapp.com/user/post-news', body);
  };
//setter
  postdata(newsItem:NewsItem) {
    this.dataSource.next(newsItem);
  }

}
