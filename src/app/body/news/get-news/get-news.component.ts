import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import {NewsService} from '../news.service';
// import {} from 

@Component({
  selector: 'app-get-news',
  templateUrl: './get-news.component.html',
  styleUrls: ['./get-news.component.css']
})
export class GetNewsComponent implements OnInit {
  newsItemData = {};
  @Input() newsData;
  @Output() deleteEvent = new EventEmitter();
  
  constructor(private newsService:NewsService) { }

  ngOnInit() {
  }

  viewNewsItem(newsItem){
    this.newsItemData = newsItem;
  }

  deleteNewsItem(id){
    alert('I am child'+id);
      this.deleteEvent.emit(id);
  }

  editNewsItem(newsItem){
    console.log(newsItem);
    //seeting the data
    this.newsService.postdata(newsItem);
  }

}
