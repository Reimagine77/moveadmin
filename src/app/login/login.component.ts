import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, Validators, FormArray   } from '@angular/forms';
import { SIGKILL } from 'constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup

  constructor() { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      "email": new FormControl(null, [Validators.required, Validators.email]),
      "password": new FormControl(null, [Validators.required, this.validatePassword]),
      "skills": new FormArray([])

    });

    // this.loginForm.setValue({
    //   'email': 'vivek',
    //   'password': 'asd'
    // });

    // this.loginForm.patchValue({
    //   'email': 'vivek'
    // });

    this.loginForm.statusChanges.subscribe(
      (data) => {
        console.log(this.loginForm.value);
      }
    );
  }

  submitForm(){
    console.log(this.loginForm);
  }

  addSkill(){
    const skill = new FormControl(null, Validators.required);
    (<FormArray>this.loginForm.get('skills')).push(skill);
  }

  validatePassword(control:FormControl):{[s:string]:boolean} {
    if(control.value == "1234"){
      return {
        'validate': true
      }
    }

    return null;

  }

}
