import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BodyComponent } from '../body/body.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { NewsComponent } from '../body/news/news.component';
import { ReviewsComponent } from '../body/reviews/reviews.component';
import { TrailersComponent } from '../body/trailers/trailers.component';
import { GalleryComponent } from '../body/gallery/gallery.component';
import { RegistrationComponent} from '../../app/registration/registration.component';
import { LoginComponent} from '../../app/login/login.component';

const routes: Routes = [
    {
        path: 'body',
        component: BodyComponent,
    },
    {
      path: 'news',
      component: NewsComponent,
    },
    {
      path: 'reviews',
      component: ReviewsComponent,
    },
    {
      path: 'trailers',
      component: TrailersComponent,
    },
    {
      path: 'gallery',
      component: GalleryComponent,
    },
    {
      path: 'registration',
      component: RegistrationComponent,
    },
    {
      path: 'login',
      component: LoginComponent,
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes
          // ,{ enableTracing: true }
        )
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }