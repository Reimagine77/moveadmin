import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NewsComponent } from './body/news/news.component';
import { ReviewsComponent } from './body/reviews/reviews.component';
import { TrailersComponent } from './body/trailers/trailers.component';
import { GalleryComponent } from './body/gallery/gallery.component';
import { GetNewsComponent } from './body/news/get-news/get-news.component';
import { PostNewsComponent } from './body/news/post-news/post-news.component';
import { UpdateNewsComponent } from './body/news/update-news/update-news.component';
import { DeleteNewsComponent } from './body/news/delete-news/delete-news.component';
import { HttpClientModule } from '@angular/common/http';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    PageNotFoundComponent,
    NewsComponent,
    ReviewsComponent,
    TrailersComponent,
    GalleryComponent,
    GetNewsComponent,
    PostNewsComponent,
    UpdateNewsComponent,
    DeleteNewsComponent,
    RegistrationComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
